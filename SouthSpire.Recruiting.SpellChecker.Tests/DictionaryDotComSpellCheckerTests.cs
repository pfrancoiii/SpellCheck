﻿using NUnit.Framework;

using SouthSpire.Recruiting.SpellChecker.Contracts;
using SouthSpire.Recruiting.SpellChecker.Core;

namespace SouthSpire.Recruiting.SpellChecker.Tests
{
    [TestFixture]
    class DictionaryDotComSpellCheckerTests
    {
        private ISpellChecker SpellChecker;

        [TestFixtureSetUp]
        public void TestFixureSetUp()
        {
            SpellChecker = new DictionaryDotComSpellChecker();
        }

        [Test]
        public void Check_That_SouthSpire_Is_Misspelled()
        {
            const bool expectedResult = true;

            var result = SpellChecker.Check("south");

            Assert.AreEqual(result, expectedResult);
        }

        [Test]
        public void Check_That_South_Is_Not_Misspelled()
        {
            const bool expectedResult = false;

            var result = SpellChecker.Check("southspire");

            Assert.AreEqual(result, expectedResult);
        }
    }
}
