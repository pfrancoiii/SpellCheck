﻿using NUnit.Framework;

using SouthSpire.Recruiting.SpellChecker.Contracts;
using SouthSpire.Recruiting.SpellChecker.Core;

namespace SouthSpire.Recruiting.SpellChecker.Tests
{
    [TestFixture]
    class MnemonicSpellCheckerIBeforeETests
    {
        private ISpellChecker SpellChecker;

        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            SpellChecker = new MnemonicSpellCheckerIBeforeE();
        }

        [Test]
        public void Check_Word_That_Contains_I_Before_E_Is_Spelled_Correctly()
        {
            const bool expectedResult = true;

            var result = SpellChecker.Check("fierce");

            Assert.AreEqual(result, expectedResult);
        }

        [Test]
        public void Check_Word_That_Contains_I_Before_E_Is_Spelled_Incorrectly()
        {
            const bool expectedResult = false;

            var result = SpellChecker.Check("science");

            Assert.AreEqual(result, expectedResult);
        }

        [Test]
        public void Check_Word_That_Contains_E_Before_I_Is_Spelled_Correctly()
        {
            const bool expectedResult = true;

            var result = SpellChecker.Check("ceiling");

            Assert.AreEqual(result, expectedResult);
        }

        [Test]
        public void Check_Word_That_Contains_E_Before_I_Is_Spelled_Incorrectly()
        {
            const bool expectedResult = false;

            var result = SpellChecker.Check("protein");

            Assert.AreEqual(result, expectedResult);
        }

        [TestCase(null)]
        public void Check_Null_Word_Is_Spelled_Correctly(string input)
        {
            const bool expectedResult = true;

            var result = SpellChecker.Check(input);

            Assert.AreEqual(result, expectedResult);
        }

        [TestCase("yes")]
        [TestCase("no")]
        public void Check_Words_That_Contain_No_I_Before_E_OR_E_Before_I_Are_Spelled_Correctly(string input)
        {
            const bool expectedResult = true;

            var result = SpellChecker.Check(input);

            Assert.AreEqual(result, expectedResult);
        }

        [TestCase("ieword")]
        public void Check_Word_That_Starts_with_I_Before_E_Is_Spelled_Correctly(string input)
        {
            const bool expectedResult = true;

            var result = SpellChecker.Check(input);

            Assert.AreEqual(result, expectedResult);
        }

        [TestCase("eiword")]
        public void Check_Word_That_Starts_with_E_Before_I_Is_Spelled_Incorrectly(string input)
        {
            const bool expectedResult = false;

            var result = SpellChecker.Check(input);

            Assert.AreEqual(result, expectedResult);
        }

        [TestCase("believefiercecolliediefrienddeceiveceilingreceipt")]
        public void Check_Word_With_Mixed_Orders_Of_E_And_I_Is_Spelled_Correctly(string input)
        {
            const bool expectedResult = true;

            var result = SpellChecker.Check(input);

            Assert.AreEqual(result, expectedResult);
        }
    }
}
