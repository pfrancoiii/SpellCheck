﻿using System;
using System.Net;
using SouthSpire.Recruiting.SpellChecker.Contracts;

namespace SouthSpire.Recruiting.SpellChecker.Core
{
    /// <summary>
    /// This is a dictionary based spell checker that uses dictionary.com to determine if
    /// a word is spelled correctly
    /// 
    /// The URL to do this looks like this: http://dictionary.reference.com/browse/<word>
    /// where <word> is the word to be checked
    /// 
    /// Example: http://dictionary.reference.com/browse/southspire would lookup the word southspire
    /// 
    /// We look for something in the response that gives us a clear indication whether the
    /// word is spelled correctly or not
    /// </summary>
    public class DictionaryDotComSpellChecker : ISpellChecker
    {
        public bool Check(string word)
        {
            var webRequest = WebRequest.Create("http://dictionary.reference.com/browse/" + word);

            try
            {
                using (HttpWebResponse webResp = (HttpWebResponse)webRequest.GetResponse())
                {
                    return webResp.StatusCode == HttpStatusCode.OK;
                }    
            }
            catch (WebException)
            {
                return false;
            }
        }
    }
}
