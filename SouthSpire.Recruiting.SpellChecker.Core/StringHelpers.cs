﻿using System;
using System.Collections.Generic;


namespace SouthSpire.Recruiting.SpellChecker.Core
{
    /// <summary>
    /// A collection of static methods that extend String
    /// </summary>
    public static class StringHelpers
    {
        /// <summary>
        /// Returns all indexes of a substring
        /// </summary>
        /// <param name="str">The string to search through</param>
        /// <param name="subStr">The substring to match within the string</param>
        /// <returns>A list of integers representing the indexes for all occurrences of a substring in a string</returns>
        public static List<int> AllIndexesOf(this string str, string subStr)
        {
            var indexes = new List<int>();

            for (var index = 0;; index = index + subStr.Length)
            {
                index = str.IndexOf(subStr, index, StringComparison.Ordinal);

                if (index == -1)
                    return indexes;

                indexes.Add(index);
            }
        }
    }
}
